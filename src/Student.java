import java.util.Date;
import java.util.List;

public class Student {
    private String name;
    private Curriculum curriculum;
    private Date startDate;
    private List<Integer> marks;

    public Student(String name, Curriculum curriculum, Date startDate, List<Integer> marks) {
        this.name = name;
        this.curriculum = curriculum;
        this.startDate = startDate;
        this.marks = marks;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Integer> getMarks() {
        return marks;
    }

    public void setMarks(List<Integer> marks) {
        this.marks = marks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}