import java.util.*;

public class TrainingCenter {
    private List<Student> students;

    public TrainingCenter(List<Student> students) {
        this.students = students;
    }

    private double getAvgMark(Student student) {
        int marksSum = student.getMarks().stream().mapToInt(i -> i).sum();
        return marksSum / (double)student.getMarks().size();
    }

    private boolean isCanFire(Student student) {
        int daysLeft = daysLeft(student);
        int maxSumMarks = daysLeft * 5 + student.getMarks().stream().mapToInt(i -> i).sum();
        return maxSumMarks / getCourseDaysDuration(student) < 4.5;
    }

    private double getCourseDaysDuration(Student student) {
        int sum = student.getCurriculum().getCourses().entrySet().stream().mapToInt(Map.Entry::getValue).sum();
        return Math.ceil(sum / 8d);
    }

    private int daysLeft(Student student) {
        int hoursLeft = hoursLeft(student);
        return (int) Math.ceil((hoursLeft) / 8d);
    }

    private int hoursLeft(Student student) {
        int marksCount = student.getMarks().size();
        int hoursSum = student.getCurriculum().getCourses().entrySet().stream().mapToInt(Map.Entry::getValue).sum();
        int hoursCompleted = marksCount * 8;
        return hoursSum - hoursCompleted;
    }

    private String getStudentInfo(Student student) {
        return student.getName() + " До окончания обучения по программе J2EE Developer осталось " + hoursLeft(student) +
                "ч.Средний балл " + getAvgMark(student) + (isCanFire(student)?" Отчислить.":" Может продолжать обучение.");
    }

    public void printStudentsInfo() {
        students.stream().map(this::getStudentInfo).forEach(System.out::println);
    }

    public void printSortedByMarks() {
        students.stream().sorted((firstStudent,secondStudent) -> getAvgMark(firstStudent) >= getAvgMark(secondStudent)?-1:1)
                .map(this::getStudentInfo).forEach(System.out::println);
    }

    public void printSortedByDate() {
        students.stream().sorted(Comparator.comparingInt(this::daysLeft))
                .map(this::getStudentInfo).forEach(System.out::println);
    }

    public static void main(String[] args) {
        HashMap<String, Integer> javaEnterpriseCourses = new HashMap<>();
        javaEnterpriseCourses.put("Технология Java Servlets", 16);
        javaEnterpriseCourses.put("Struts Framework", 24);
        javaEnterpriseCourses.put("Spring Framework", 48);
        javaEnterpriseCourses.put("Hibernate", 20);

        HashMap<String, Integer> javaCourses = new HashMap<>();
        javaCourses.put("Обзор технологий Java", 8);
        javaCourses.put("Библиотека JFC/Swing", 16);
        javaCourses.put("Технология JDBC", 16);
        javaCourses.put("Технология JAX", 52);
        javaCourses.put("Библиотеки commons", 44);

        Curriculum javaCurriculum = new Curriculum(javaCourses,"Java Developer");
        Curriculum javaEnterpriseCurriculum = new Curriculum(javaEnterpriseCourses,"J2EE Developer");

        ArrayList<Student> students = new ArrayList<>();
        List<Integer> ivanMarks = Arrays.asList(1,1,1,1,2,3,3,3,3,3,3);
        List<Integer> alexMarks = Arrays.asList(5,5,3,4,3,5,4,5,5);
        List<Integer> petrMarks = Arrays.asList(3,4,5,3,4,5,5,4,5,4,5);

        Student ivan = new Student("Ivan Ivanov",javaCurriculum,getStartDate(2017,1,1),ivanMarks);
        Student petr = new Student("Petr Petrov",javaEnterpriseCurriculum,getStartDate(2017,2,1),petrMarks);
        Student alex = new Student("Alex Popov", javaEnterpriseCurriculum, getStartDate(2017, 3, 1), alexMarks);
        students.add(alex);
        students.add(petr);
        students.add(ivan);

        TrainingCenter trainingCenter = new TrainingCenter(students);
        trainingCenter.printSortedByMarks();
    }

    private static Date getStartDate(int year, int month, int day) {
        return new GregorianCalendar(year,month,day).getTime();
    }
}