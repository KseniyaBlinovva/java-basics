import java.util.HashMap;
import java.util.Map;

public class Curriculum {
    private Map<String,Integer> courses = new HashMap<>();
    private String name;

    public Curriculum(Map<String, Integer> courses, String name) {
        this.courses = courses;
        this.name = name;
    }

    public Map<String, Integer> getCourses() {
        return courses;
    }

    public void setCourses(Map<String, Integer> courses) {
        this.courses = courses;
    }

    public void addCourse(String courseName, int duration) {
        courses.put(courseName,duration);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
